import 'dart:convert';

import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:rental_mm/util/session.dart';
import 'user.dart';

class Mobil {
  final String nama_mobil, plat_mobil;
  final int idMobil, tarif, status;
  late User pemilik; // Tambahkan properti pemilik

  Mobil({
    required this.idMobil,
    required this.nama_mobil,
    required this.plat_mobil,
    required this.tarif,
    required this.status,
  });

  factory Mobil.fromJson(Map<String, dynamic> json) {
    return Mobil(
      idMobil: json['id_mobil'],
      nama_mobil: json['nama_mobil'],
      plat_mobil: json['plat_mobil'],
      tarif: json['tarif'],
      status: json['status'],
    );
  }

}




List<Mobil> mobilFromJson(jsonData) {
  List<Mobil> result =
  List<Mobil>.from(jsonData.map((item) => Mobil.fromJson(item)));

  return result;
}

Future<List<Mobil>> fetchMobil() async {
  final response = await http.get(Uri.parse('https://rentalmm.000webhostapp.com/mobil/index.php'));

  if (response.statusCode == 200) {
    var jsonResp = json.decode(response.body);

    final userResponse = await http.get(Uri.parse('https://rentalmm.000webhostapp.com/user/index.php'));
    if (userResponse.statusCode == 200) {
      var userJsonResp = json.decode(userResponse.body);
      List<Mobil> mobilList = [];

      for (var mobilJson in jsonResp) {
        var ownerId = mobilJson['id_user'];
        var ownerData = userJsonResp.firstWhere((user) => user['id_user'] == ownerId, orElse: () => {});

        // Checking if ownerData is not empty
        if (ownerData.isNotEmpty) {
          User owner = User.fromJson(ownerData);
          Mobil mobil = Mobil.fromJson(mobilJson);
          mobil.pemilik = owner;
          mobilList.add(mobil);
        }
      }
      return mobilList;
    } else {
      throw Exception('Failed to load user data, status: ${userResponse.statusCode}');
    }
  } else {
    throw Exception('Failed to load mobil data, status : ${response.statusCode}');
  }
}

Future<List<Mobil>> fetchMobilPerental() async {
  final prefs = await SharedPreferences.getInstance();
  int idUser = prefs.getInt(ID_USER) ?? 0;
  final response = await http.get(Uri.parse('https://rentalmm.000webhostapp.com/mobil/indexperental.php?id_user=$idUser'));

  if (response.statusCode == 200) {
    var jsonResp = json.decode(response.body);
    return mobilFromJson(jsonResp);
  } else {
    throw Exception('Failed load, status : ${response.statusCode}');
  }
}

Future mobilCreate(Mobil mobil) async {
  //String route = AppConfig.API_ENDPOINT + "/pasien/create.php"
  final prefs = await SharedPreferences.getInstance();
  int idUser = prefs.getInt(ID_USER) ?? 0;
  try {
    final response = await http.post(Uri.parse('https://rentalmm.000webhostapp.com/mobil/create.php'),
        headers: {"Content-Type": "application/json"},
        body:
        jsonEncode({
          'id_user': idUser,
          'nama_mobil': mobil.nama_mobil,
          'plat_mobil': mobil.plat_mobil,
          'tarif': mobil.tarif}));

    return response;
  } catch (e) {
    print("Error : ${e.toString()}");
    return null;
  }
}





