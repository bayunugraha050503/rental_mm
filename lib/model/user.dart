import 'dart:convert';

import 'package:http/http.dart';
import 'package:http/http.dart' as http;

class User {
  //final String idUser, username, password;
  //final Pasien idPasien;

  final String nama, email, ktp, hp, password;
  final int idUser;
  final dynamic otoritas;

  //User( {required this.idUser, required this.username, required this.password, required this.idPasien});
  User( {required this.nama,  required this.email, required this.ktp, required this.hp, required this.password, required this.idUser, required this.otoritas});


  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        idUser: json['id_user'],
        nama: json['nama'],
        email: json['email'],
        ktp: json['ktp'],
        hp: json['hp'],
        password: json['password'],
        otoritas: json['otoritas']);
  }

/*factory User.fromJson(Map<String, dynamic> json) {
    return User(
        username: json['username'],
        password: json['password'],
    );
  }*/

// get idPasien => null;
}

// login (POST)
Future<Response?> login(User user) async {
  //String route = AppConfig.API_ENDPOINT + "/login.php";
  try {
    final response = await http.post(Uri.parse('https://rentalmm.000webhostapp.com/login.php'),
        headers: {"Content-Type": "application/json"},
        body:
        jsonEncode({'email': user.email, 'password': user.password, 'otoritas': user.otoritas}));

    print(response.body.toString());

    return response;
  } catch (e) {
    print("Error : ${e.toString()}");
    return null;
  }
}

Future userCreate(User user) async {
  //String route = AppConfig.API_ENDPOINT + "/pasien/create.php";
  try {
    final response = await http.post(Uri.parse('https://rentalmm.000webhostapp.com/user/create.php'),
        headers: {"Content-Type": "application/json"},
        body:
        jsonEncode({'nama': user.nama, 'email': user.email, 'ktp': user.ktp, 'hp': user.hp, 'password': user.password, 'otoritas': user.otoritas}));

    return response;
  } catch (e) {
    print("Error : ${e.toString()}");
    return null;
  }
}


