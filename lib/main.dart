import 'package:flutter/material.dart';
import 'page/login.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:rental_mm/page/modul_konsumen/index.dart' as IndexKonsumen;
import 'package:rental_mm/page/modul_perental/index.dart' as IndexPerental;
import 'package:rental_mm/page/modul_admin/index.dart' as IndexAdmin;
import 'package:rental_mm/util/session.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      // theme: ThemeData(primarySwatch: Colors.green),
      home: FutureBuilder(
        future: _loadSession(),
        builder: (context, snapshot) {
          late final SharedPreferences prefs = snapshot.data;
          late Widget result;
          if (snapshot.connectionState == ConnectionState.waiting) {
            result = Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasData) {
              if (prefs.getBool(IS_LOGIN) ?? false) {
                if (prefs.getString(JENIS_LOGIN) ==
                    jenisLogin.KONSUMEN.toString()) {
                  result = IndexKonsumen.IndexPage();
                } else if (prefs.getString(JENIS_LOGIN) ==
                    jenisLogin.PERENTAL.toString()) {
                  result = IndexPerental.IndexPage();
                } else {
                  result = IndexAdmin.IndexPage();
                }
              } else {
                result = SliderLoginPage();
              }
            } else {
              return Container(child: Text('Error..'));
            }
          }

          return result;
        },
      ),
    );
  }

  Future _loadSession() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs;
  }
}

