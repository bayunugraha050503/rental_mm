import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:rental_mm/page/login.dart';
import 'package:rental_mm/util/session.dart';



void dialog(BuildContext _context, dynamic msg, {String? title}) {
  showDialog(
    context: _context,
    builder: (BuildContext context) {
      return Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        child: Container(
          padding: EdgeInsets.all(20.0),
          decoration: BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: const Offset(0.0, 10.0),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              title != null
                  ? Text(
                title,
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              )
                  : Container(),
              SizedBox(height: 16.0),
              Text(
                msg.toString(),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 16.0,
                ),
              ),
              SizedBox(height: 24.0),
              Align(
                alignment: Alignment.bottomCenter,
                child: TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    'Tutup',
                    style: TextStyle(
                      color: Colors.blue,
                      fontSize: 18.0,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    },
  );
}


//
// // Template button lebar utk posisi bottomNavigationBar
// Widget largetButton(
//     {String label = "Simpan", IconData? iconData, Function? onPressed}) {
//   iconData = iconData ?? Icons.done_all;
//   return Container(
//     height: 60,
//     width: double.infinity,
//     child: ElevatedButton.icon(
//         style: ElevatedButton.styleFrom(elevation: 4.0, backgroundColor: Colors.blue,
//           disabledForegroundColor: Colors.grey.withOpacity(0.38),
//           disabledBackgroundColor: Colors.grey.withOpacity(0.12),),
//         label: Text(
//           label,
//           style: TextStyle(
//             color: Colors.white,
//             fontSize: 16.0,
//           ),
//         ),
//
//         icon: Icon(iconData, color: Colors.white),
//
//         onPressed: (){ }),
//   );
// }
//
// // fungsi format tulisan rupiah
// String toRupiah(int val) {
//   final formatter = NumberFormat.currency(locale: 'ID');
//   return formatter.format(val);
// }
//
void logOut(BuildContext context) {
  clearSession().then((value) => Navigator.pushAndRemoveUntil(
      context,
      PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation,
          Animation secondaryAnimation) {
        return LoginPage(parentContext: context,);
      }, transitionsBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secondaryAnimation, Widget child) {
        return new SlideTransition(
          position: new Tween<Offset>(
            begin: const Offset(1.0, 0.0),
            end: Offset.zero,
          ).animate(animation),
          child: child,
        );
      }),
          (route) => false));
}
