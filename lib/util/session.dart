import 'package:shared_preferences/shared_preferences.dart';
import 'package:rental_mm/model/user.dart';

const IS_LOGIN = 'IS_LOGIN';
const JENIS_LOGIN = 'JENIS_LOGIN';
const ID_USER = 'ID_USER';
const NAMA = 'NAMA';
const EMAIL = 'EMAIL';
const KTP = 'KTP';
const HP = 'HP';

// ignore: camel_case_types
enum jenisLogin { ADMIN, KONSUMEN, PERENTAL }

Future createKonsumenSessionx(int id, String nama, String email, String ktp, String hp) async {
  final prefs = await SharedPreferences.getInstance();
  prefs.setBool(IS_LOGIN, true);
  prefs.setInt(ID_USER, id);
  prefs.setString(NAMA, nama);
  prefs.setString(EMAIL, email);
  prefs.setString(KTP, ktp);
  prefs.setString(HP, hp);
  prefs.setString(JENIS_LOGIN, jenisLogin.KONSUMEN.toString());
  return true;
}

Future createPerentalSessionx(int id, String nama, String email, String ktp, String hp) async {
  final prefs = await SharedPreferences.getInstance();
  prefs.setBool(IS_LOGIN, true);
  prefs.setInt(ID_USER, id);
  prefs.setString(NAMA, nama);
  prefs.setString(EMAIL, email);
  prefs.setString(KTP, ktp);
  prefs.setString(HP, hp);
  prefs.setString(JENIS_LOGIN, jenisLogin.PERENTAL.toString());
  return true;
}

Future createAdminSession(int id, String email) async {
  final prefs = await SharedPreferences.getInstance();
  prefs.setBool(IS_LOGIN, true);
  prefs.setInt(ID_USER, id);
  prefs.setString(EMAIL, email);
  prefs.setString(JENIS_LOGIN, jenisLogin.ADMIN.toString());
  return true;
}

Future clearSession() async {
  final prefs = await SharedPreferences.getInstance();
  prefs.clear();
  return true;
}
