import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:rental_mm/model/user.dart';
import 'package:rental_mm/page/login.dart';
import 'package:rental_mm/util/util.dart';

class RegistrasiPage extends StatefulWidget {
  @override
  _RegistrasiPageState createState() => _RegistrasiPageState();
}

class _RegistrasiPageState extends State<RegistrasiPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController namaCont = new TextEditingController();
  TextEditingController emailCont = new TextEditingController();
  TextEditingController ktpCont = new TextEditingController();
  TextEditingController hpCont = new TextEditingController();
  TextEditingController passCont = new TextEditingController();
  late String _selectedOtoritas;
  List<String> otoritas = ["Merental mobil", "Merentalkan mobil Anda"];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(20.0),
        child: _formWidget(context),
      ),
    );
  }
  Widget _formWidget(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextFormField(
                controller: namaCont,
                decoration: InputDecoration(
                  labelText: 'Nama',
                  border: OutlineInputBorder(),
                  prefixIcon: Icon(Icons.person),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Tidak boleh kosong';
                  }
                  return null;
                },
              ),
              SizedBox(height: 20.0),
              TextFormField(
                controller: emailCont,
                decoration: InputDecoration(
                  labelText: 'Email',
                  border: OutlineInputBorder(),
                  prefixIcon: Icon(Icons.email),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Tidak boleh kosong';
                  }
                  return null;
                },
              ),
              SizedBox(height: 20.0),
              TextFormField(
                controller: ktpCont,
                decoration: InputDecoration(
                  labelText: 'KTP',
                  border: OutlineInputBorder(),
                  prefixIcon: Icon(Icons.credit_card),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Tidak boleh kosong';
                  }
                  return null;
                },
              ),
              SizedBox(height: 20.0),
              TextFormField(
                controller: hpCont,
                decoration: InputDecoration(
                  labelText: 'Nomor HP',
                  border: OutlineInputBorder(),
                  prefixIcon: Icon(Icons.phone),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Tidak boleh kosong';
                  }
                  return null;
                },
              ),
          SizedBox(height: 20.0),
          _dropPoli(),
          SizedBox(height: 20.0),
              TextFormField(
                controller: passCont,
                obscureText: true,
                decoration: InputDecoration(
                  labelText: 'Password',
                  border: OutlineInputBorder(),
                  prefixIcon: Icon(Icons.lock),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Tidak boleh kosong';
                  }
                  return null;
                },
              ),
          SizedBox(height: 20.0),
              ElevatedButton(
                onPressed: prosesRegistrasi,
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.blue, // Warna latar belakang
                  foregroundColor: Colors.white, // Warna teks
                  padding: EdgeInsets.symmetric(horizontal: 50, vertical: 15),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
                child: Text(
                  'Daftar',
                  style: TextStyle(fontSize: 18),
                ),
              ),
              SizedBox(height: 15.0),
              TextButton(
                onPressed: () => Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => LoginPage(parentContext: context)),
                ),
                style: TextButton.styleFrom(
                  primary: Colors.blue, // Warna teks
                ),
                child: Text('Sudah punya akun? Login'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _dropPoli() {
    return DropdownButtonFormField(
      decoration: InputDecoration(
        border: OutlineInputBorder(),
      ),
      hint: Text("Anda menggunakan aplikasi untuk?"),
      items: otoritas.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(value: value, child: Text(value));
      }).toList(),
      onChanged: (value) {
        setState(() {
          _selectedOtoritas = value!;
        });
      },
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Harap pilih opsi';
        }
        return null;
      },
    );
  }

  void prosesRegistrasi() async {
    if (_formKey.currentState!.validate()) {
      if (_selectedOtoritas == "Merental mobil") {
        _selectedOtoritas = "1";
      } else if (_selectedOtoritas == "Merentalkan mobil Anda") {
        _selectedOtoritas = "2";
      }

      final response = await userCreate(User(
          nama: namaCont.text,
          email: emailCont.text,
          ktp: ktpCont.text,
          hp: hpCont.text,
          password: passCont.text,
          idUser: 0,
          otoritas: int.parse(_selectedOtoritas)));

      if (response != null) {
        print(response.body.toString());
        if (response.statusCode == 200) {
          var jsonResp = json.decode(response.body);
          Navigator.pop(context, jsonResp['message']);
        } else {
          dialog(context, "${response.body.toString()}");
        }
      }
    } else {
      dialog(context, "Harap lengkapi form pendaftaran");
    }
  }
}
