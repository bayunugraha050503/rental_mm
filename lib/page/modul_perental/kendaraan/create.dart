import 'package:flutter/material.dart';
import 'package:rental_mm/page/modul_perental/kendaraan/kendaraan_content.dart';
import 'package:rental_mm/model/mobil.dart';
import 'package:flutter/services.dart';
import 'dart:convert';
import 'package:rental_mm/util/util.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:rental_mm/util/session.dart';

class CreatePage extends StatefulWidget {

  @override
  _CreatePageState createState() => _CreatePageState();
}

class _CreatePageState extends State<CreatePage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController namaCont = new TextEditingController();
  TextEditingController platCont = new TextEditingController();
  TextEditingController tarifCont = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tambah mobil'),
      ),
      body: Padding(
        padding: EdgeInsets.all(20.0),
        child: _formWidget(context),
      ),
    );
  }
  Widget _formWidget(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextFormField(
              controller: namaCont,
              decoration: InputDecoration(
                labelText: 'Nama mobil',
                border: OutlineInputBorder(),
                prefixIcon: Icon(Icons.drive_eta),
              ),
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Tidak boleh kosong';
                }
                return null;
              },
            ),
            SizedBox(height: 20.0),
            TextFormField(
              controller: platCont,
              decoration: InputDecoration(
                labelText: 'Plat mobil',
                border: OutlineInputBorder(),
                prefixIcon: Icon(Icons.align_horizontal_left),
              ),
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Tidak boleh kosong';
                }
                return null;
              },
            ),
            SizedBox(height: 20.0),
            TextFormField(
              controller: tarifCont,
              decoration: InputDecoration(
                labelText: 'Tarif per hari',
                border: OutlineInputBorder(),
                prefixIcon: Icon(Icons.attach_money),
              ),
              keyboardType: TextInputType.number, // Mengatur keyboard agar hanya menampilkan angka
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.digitsOnly // Memfilter hanya angka yang diterima
              ],
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Tidak boleh kosong';
                }
                return null;
              },
            ),
            SizedBox(height: 20.0),
            ElevatedButton(
              onPressed: _saveData,
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.blue, // Warna latar belakang
                foregroundColor: Colors.white, // Warna teks
                padding: EdgeInsets.symmetric(horizontal: 50, vertical: 15),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
              ),
              child: Text(
                'Tambahkan',
                style: TextStyle(fontSize: 18),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _saveData() async {
    if (_formKey.currentState!.validate()) {
      int tarif = int.parse(tarifCont.text);
      Mobil mobil = new Mobil(
          idMobil: 0,
          nama_mobil: namaCont.text,
          plat_mobil: platCont.text,
          tarif: tarif,
          status: 0
      );

      final response = await mobilCreate(mobil);
      if (response != null) {
        print(response.body.toString());
        if (response.statusCode == 200) {
          var jsonResp = json.decode(response.body);
          Navigator.pop(context, jsonResp['message']);
        } else {
          print(response.statusCode);
          print(response.body.toString());
          dialog(context, response.body.toString());
        }
      }
    } else {
      dialog(context, "Harap lengkapi form mobil");
    }
  }
}
