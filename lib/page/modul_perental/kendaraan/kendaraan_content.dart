import 'package:flutter/material.dart';
import 'package:rental_mm/model/mobil.dart';
import 'package:rental_mm/page/modul_perental/kendaraan/create.dart' as KendaraanCreate;
import 'package:rental_mm/util/util.dart';

class KendaraanContent extends StatefulWidget {
  @override
  _KendaraanContentState createState() => _KendaraanContentState();
}

class _KendaraanContentState extends State<KendaraanContent> {
  late Future<List<Mobil>> _mobilData;

  @override
  void initState() {
    super.initState();
    _mobilData = fetchMobilPerental();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
        onPressed: () async {
      final result = await Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => KendaraanCreate.CreatePage()));

      if (result != null) {
        dialog(context, result);
        setState(() {
          _mobilData = fetchMobilPerental();
        });
      }
    },
    backgroundColor: Colors.blue,
    child: Icon(Icons.add)),
    body: Center(
    child: FutureBuilder<List<Mobil>>(
      future: _mobilData,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else if (snapshot.hasError) {
          return Center(
            child: Text('Error: ${snapshot.error}'),
          );
        } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
          return Center(
            child: Text('No data available'),
          );
        } else {
          return ListView.builder(
            itemCount: snapshot.data!.length,
            itemBuilder: (context, index) {
              return _buildMobilCard(snapshot.data![index]);
            },
          );
        }
      },
    ),
    ),
    );
  }


  Widget _buildMobilCard(Mobil mobil) {
    Color statusColor = (mobil.status == 0) ? Colors.green : Colors.red;
    String statusText = (mobil.status == 0) ? 'Tersedia' : 'Sedang dirental';

    return Card(
      elevation: 5,
      margin: EdgeInsets.all(8.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: ListTile(
        contentPadding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 24.0),
        title: Text(
          mobil.nama_mobil,
          style: TextStyle(
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 8.0),
            Text(
              'Plat: ${mobil.plat_mobil}',
              style: TextStyle(
                fontSize: 16.0,
              ),
            ),
            SizedBox(height: 4.0),
            Text(
              'Tarif: Rp${mobil.tarif} per hari',
              style: TextStyle(
                fontSize: 16.0,
              ),
            ),
          ],
        ),
        trailing: Text(
          statusText,
          style: TextStyle(
            color: statusColor,
            fontWeight: FontWeight.bold,
            fontSize: 16.0,
          ),
        ),
      ),
    );
  }


}
