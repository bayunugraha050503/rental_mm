import 'package:flutter/material.dart';
import 'package:rental_mm/model/user.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:rental_mm/util/session.dart';
import 'package:rental_mm/util/util.dart';

class ProfilContent extends StatefulWidget {
  @override
  _ProfilContentState createState() => _ProfilContentState();
}

class _ProfilContentState extends State<ProfilContent> {
  late User loggedInPerental = User(
    idUser: 0,
    nama: '',
    email: '',
    ktp: '',
    hp: '',
    password: '',
    otoritas: jenisLogin.PERENTAL,
  );

  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    loadLoggedInPerental();
  }

  Future<void> loadLoggedInPerental() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int idUser = prefs.getInt(ID_USER) ?? 0;
    String nama = prefs.getString(NAMA) ?? '';
    String email = prefs.getString(EMAIL) ?? '';
    String ktp = prefs.getString(KTP) ?? '';
    String hp = prefs.getString(HP) ?? '';

    loggedInPerental = User(
      idUser: idUser,
      nama: nama,
      email: email,
      ktp: ktp,
      hp: hp,
      password: '', // Isi dengan password jika tersedia
      otoritas: jenisLogin.PERENTAL,
    );

    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: _isLoading
          ? CircularProgressIndicator()
          : Container(
        width: MediaQuery.of(context).size.width * 0.8,
        child: Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: Padding(
            padding: EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Center( // Tambahkan Center di sekitar teks 'Profil Anda'
                  child: Text(
                    'Profil Anda',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0,
                      color: Colors.black, // Ubah warna teks judul
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                _buildProfileDetail('Nama', loggedInPerental.nama),
                _buildProfileDetail('Email', loggedInPerental.email),
                _buildProfileDetail('Nomor KTP', loggedInPerental.ktp),
                _buildProfileDetail('Nomor HP', loggedInPerental.hp),
                // Tambahkan informasi lainnya sesuai kebutuhan dari User model
                // ...
                SizedBox(height: 20.0),
                Center(
                child: ElevatedButton.icon(
                  onPressed: () => logOut(context),
                  icon: Icon(Icons.logout, color: Colors.white),
                  label: Text(
                    'Logout',
                    style: TextStyle(color: Colors.white),
                  ),
                  style: ElevatedButton.styleFrom(
                    primary: Colors.red, // Warna latar merah
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                  ),
                ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildProfileDetail(String label, String value) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            label,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.grey,
            ),
          ),
          SizedBox(height: 4.0),
          Text(
            value,
            style: TextStyle(
              fontSize: 16.0,
              // Atur ukuran teks detail
            ),
          ),
        ],
      ),
    );
  }
}
