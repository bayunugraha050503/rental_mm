import 'package:flutter/material.dart';
import 'package:rental_mm/model/user.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:rental_mm/util/session.dart';
import 'package:rental_mm/util/util.dart';

class ProfilContent extends StatefulWidget {
  @override
  _ProfilContentState createState() => _ProfilContentState();
}

class _ProfilContentState extends State<ProfilContent> {
  late User loggedInKonsumen = User(
    idUser: 0,
    nama: '',
    email: '',
    ktp: '',
    hp: '',
    password: '',
    otoritas: jenisLogin.KONSUMEN,
  );

  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    loadLoggedInKonsumen();
  }

  Future<void> loadLoggedInKonsumen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int idUser = prefs.getInt(ID_USER) ?? 0;
    String nama = prefs.getString(NAMA) ?? '';
    String email = prefs.getString(EMAIL) ?? '';
    String ktp = prefs.getString(KTP) ?? '';
    String hp = prefs.getString(HP) ?? '';

    loggedInKonsumen = User(
      idUser: idUser,
      nama: nama,
      email: email,
      ktp: ktp,
      hp: hp,
      password: '', // Isi dengan password jika tersedia
      otoritas: jenisLogin.KONSUMEN, // Isi dengan jenis login konsumen
    );

    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: _isLoading
          ? CircularProgressIndicator()
          : Container(
        width: MediaQuery.of(context).size.width * 0.8,
        child: Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: Padding(
            padding: EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Center(
                  child: Text(
                    'Profil Anda',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0,
                      color: Colors.black,
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                _buildProfileDetail('Nama', loggedInKonsumen.nama),
                _buildProfileDetail('Email', loggedInKonsumen.email),
                _buildProfileDetail('Nomor KTP', loggedInKonsumen.ktp),
                _buildProfileDetail('Nomor HP', loggedInKonsumen.hp),
                SizedBox(height: 20.0),
                Center(
                  child: ElevatedButton.icon(
                    onPressed: () => logOut(context),
                    icon: Icon(Icons.logout, color: Colors.white),
                    label: Text(
                      'Logout',
                      style: TextStyle(color: Colors.white),
                    ),
                    style: ElevatedButton.styleFrom(
                      primary: Colors.red,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildProfileDetail(String label, String value) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            label,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.grey,
            ),
          ),
          SizedBox(height: 4.0),
          Text(
            value,
            style: TextStyle(
              fontSize: 16.0,
            ),
          ),
        ],
      ),
    );
  }
}
