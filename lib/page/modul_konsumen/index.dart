import 'package:flutter/material.dart';
import 'package:rental_mm/page/modul_konsumen/home/home_content.dart';
import 'package:rental_mm/page/modul_konsumen/profil/profil_content.dart';
import 'package:rental_mm/page/modul_konsumen/riwayat/riwayat_content.dart';
import 'package:rental_mm/util/util.dart';

class IndexPage extends StatefulWidget {
  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  int _selectedIndex = 1;
  Widget _selectedContent = HomeContent();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _selectedContent,
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
        topRight: Radius.circular(30), topLeft: Radius.circular(30)),
        boxShadow: [
         BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 10),
        ],
         ),
        child: ClipRRect(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30.0),
          topRight: Radius.circular(30.0),
        ),
        child: BottomNavigationBar(
          currentIndex: _selectedIndex,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.history),
              label: 'Riwayat',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: 'Profil',
            ),
          ],
          onTap: _onItemTapped,
          selectedLabelStyle: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
    ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      switch (index) {
        case 0:
          _selectedContent = RiwayatContent();
          break;

        case 1:
          _selectedContent = HomeContent();
          break;

        case 2:
          _selectedContent = ProfilContent();
          break;
      }
    });
  }
}
