import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:rental_mm/model/user.dart';
import 'package:rental_mm/page/modul_konsumen/index.dart' as IndexKonsumen;
// import 'package:good_health/page/modul_pasien/signup.dart';
import 'package:rental_mm/page/modul_perental/index.dart' as IndexPerental;
import 'package:rental_mm/page/modul_admin/index.dart' as IndexAdmin;
import 'package:rental_mm/util/session.dart';
import 'package:rental_mm/util/util.dart';
import 'package:rental_mm/page/registrasi.dart';
import 'dart:developer' as developer;

class SliderLoginPage extends StatefulWidget {
  @override
  _SliderLoginPageState createState() => _SliderLoginPageState();
}

class _SliderLoginPageState extends State<SliderLoginPage> {

  final List<String> sliderImages = [
    'assets/image1.svg',
    'assets/image2.svg',
    'assets/image3.svg',
  ];

  final List<String> sliderTexts = [
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et aliquam massa.',
    'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.',
    'Integer auctor nunc nec nulla bibendum, quis fringilla sapien vestibulum.'
  ];

  int _currentIndex = 0;
  final CarouselController _carouselController = CarouselController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CarouselSlider(
                carouselController: _carouselController,
                options: CarouselOptions(
                  height: 300.0,
                  enlargeCenterPage: true,
                  enableInfiniteScroll: false,
                  onPageChanged: (index, _) {
                    setState(() {
                      _currentIndex = index;
                    });
                  },
                ),
                items: sliderImages.asMap().entries.map((entry) {
                  int index = entry.key;
                  String image = entry.value;
                  return Builder(
                    builder: (BuildContext context) {
                      return Column(
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.symmetric(horizontal: 5.0),
                            child: AspectRatio(
                              aspectRatio: 16 / 9,
                              child: SvgPicture.asset(
                                image,
                                fit: BoxFit.contain,
                              ),
                            ),
                          ),
                          SizedBox(height: 20.0),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 20.0),
                            child: Text(
                              sliderTexts[index],
                              style: TextStyle(fontSize: 16.0),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      );
                    },
                  );
                }).toList(),
              ),
              SizedBox(height: 20.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  for (int i = 0; i < sliderImages.length; i++)
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 4.0),
                      width: 8.0,
                      height: 8.0,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: _currentIndex == i ? Colors.blue : Colors.grey,
                      ),
                    ),
                ],
              ),
            ],
          ),
          if (_currentIndex > 0)
            Positioned(
              bottom: 20.0,
              left: 20.0,
              child: GestureDetector(
                onTap: () {
                  _carouselController.previousPage();
                },
                child: Align(
                  alignment: Alignment.bottomLeft,
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      'Kembali',
                      style: TextStyle(fontSize: 18.0, color: Colors.blue),
                    ),
                  ),
                ),
              ),
            ),
          if (_currentIndex < sliderImages.length - 1)
            Positioned(
              bottom: 20.0,
              right: 20.0,
              child: GestureDetector(
                onTap: () {
                  _carouselController.nextPage();
                },
                child: Align(
                  alignment: Alignment.bottomRight,
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      'Selanjutnya',
                      style: TextStyle(fontSize: 18.0, color: Colors.blue),
                    ),
                  ),
                ),
              ),
            ),
          if (_currentIndex == sliderImages.length - 1)
            Positioned(
              bottom: 20.0,
              right: 20.0,
              child: GestureDetector(
                onTap: () {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => LoginPage(parentContext: context,),
                    ),
                  );
                },
                child: Align(
                  alignment: Alignment.bottomRight,
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      'Login',
                      style: TextStyle(fontSize: 18.0, color: Colors.blue),
                    ),
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
}


class LoginPage extends StatelessWidget {
  final BuildContext parentContext; // Tambahkan variabel context
  LoginPage({required this.parentContext});

  final _formKey = GlobalKey<FormState>();
  TextEditingController emailCont = new TextEditingController();
  TextEditingController passCont = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(20.0),
        child: _formWidget(context),
      ),
    );
  }

  Widget _formWidget(BuildContext context) {
    return Form(
      key: _formKey,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextFormField(
            controller: emailCont,
            decoration: InputDecoration(
              labelText: 'Email',
              border: OutlineInputBorder(),
              prefixIcon: Icon(Icons.email),
            ),
            validator: (value) {
              if (value!.isEmpty) {
                return 'Tidak boleh kosong';
              }
              return null;
            },
          ),
          SizedBox(height: 20.0),
          TextFormField(
            controller: passCont,
            obscureText: true,
            decoration: InputDecoration(
              labelText: 'Password',
              border: OutlineInputBorder(),
              prefixIcon: Icon(Icons.lock),
            ),
            validator: (value) {
              if (value!.isEmpty) {
                return 'Tidak boleh kosong';
              }
              return null;
            },
          ),
          SizedBox(height: 20.0),
          ElevatedButton(
            onPressed: tes_login,
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.blue, // Warna latar belakang
              foregroundColor: Colors.white, // Warna teks
              padding: EdgeInsets.symmetric(horizontal: 50, vertical: 15),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
              ),
            ),
            child: Text(
              'Login',
              style: TextStyle(fontSize: 18),
            ),
          ),
          SizedBox(height: 15.0),
          TextButton(
            onPressed: () async {
              final result = await Navigator.of(parentContext).push(
                MaterialPageRoute(builder: (context) => RegistrasiPage()),
              );

              if (result != null) {
                dialog(parentContext, result);
              }
            },
            style: TextButton.styleFrom(
              primary: Colors.blue, // Warna teks
            ),
            child: Text('Belum punya akun? Daftar di sini'),
          ),
        ],
      ),
    );
  }

  void tes_login() async {
    if (_formKey.currentState!.validate()) {
      try {
        // var idPasien = null;
        final response = await login(
            User(nama: '',
                email: emailCont.text,
                ktp: '',
                hp: '',
                password: passCont.text,
                idUser: 0,
                otoritas: 1));
        // User(username: usernameCont.text, password: passCont.text));
        var jsonResp = json.decode(response!.body);
        if (response.statusCode == 200) {
          //  User user = User.fromJson(jsonResp['user']);

          developer.log(jsonResp['user']['email']);

          if (jsonResp['user']['otoritas'] == 1) {
            createKonsumenSessionx(
                jsonResp['user']['id_user'], jsonResp['user']['nama'],
                jsonResp['user']['email'], jsonResp['user']['ktp'],
                jsonResp['user']['hp']);

            Navigator.pushReplacement(parentContext,
                MaterialPageRoute(
                    builder: (context) => IndexKonsumen.IndexPage()));
          } else if (jsonResp['user']['otoritas'] == 2) {
            createPerentalSessionx(
                jsonResp['user']['id_user'], jsonResp['user']['nama'],
                jsonResp['user']['email'], jsonResp['user']['ktp'],
                jsonResp['user']['hp']);

            Navigator.pushReplacement(parentContext,
                MaterialPageRoute(
                    builder: (context) => IndexPerental.IndexPage()));
          } else {
            createAdminSession(jsonResp['user']['id_user'],jsonResp['user']['email']);
            Navigator.pushReplacement(
                parentContext,
                MaterialPageRoute(
                    builder: (context) => IndexAdmin.IndexPage()));
            dialog(parentContext, jsonResp['user']['email']);
          }
        } else if (response.statusCode == 401) {
          dialog(parentContext, jsonResp['message']);
        } else {
          dialog(parentContext, response.body.toString());
        }
      } catch (e) {
        // dialog(parentContext, e.toString());
        dialog(parentContext, 'Username atau password salah');
        //developer.log('tes tes');
      }
    } else {
      dialog(parentContext, 'Harap lengkapi form login');
    }
  }
}
