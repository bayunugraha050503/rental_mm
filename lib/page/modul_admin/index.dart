import 'package:flutter/material.dart';
import 'package:rental_mm/page/modul_admin/kendaraan/kendaraan_content.dart';
import 'package:rental_mm/page/modul_admin/rentalan/rentalan_content.dart';
import 'package:rental_mm/util/util.dart';

class IndexPage extends StatefulWidget {
  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  int _selectedIndex = 1;
  Widget _selectedContent = RentalanContent();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Admin"),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.exit_to_app,
              color: Colors.white,
            ),
            onPressed: () => logOut(context),
          ),
        ],
      ),
      body: _selectedContent,
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(30), topLeft: Radius.circular(30)),
          boxShadow: [
            BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 10),
          ],
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30.0),
            topRight: Radius.circular(30.0),
          ),
          child: BottomNavigationBar(
            currentIndex: _selectedIndex,
            type: BottomNavigationBarType.fixed,
            items: [
              BottomNavigationBarItem(
                icon: Icon(Icons.drive_eta),
                label: 'Kendaraan',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.car_rental),
                label: 'Konfirmasi rental',
              ),
            ],
            onTap: _onItemTapped,
            selectedLabelStyle: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      switch (index) {
        case 0:
          _selectedContent = KendaraanContent();
          break;

        case 1:
          _selectedContent = RentalanContent();
          break;
      }
    });
  }
}
